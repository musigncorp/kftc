document.addEventListener("DOMContentLoaded", function () {
    var radioButtons = document.querySelectorAll('input[type="radio"][name="corp"]');

    radioButtons.forEach(function (radioButton) {
        radioButton.addEventListener('change', function () {
            document.querySelectorAll('.corp_wrap').forEach(function (corpWrap) {
                corpWrap.classList.remove('active');
            });

            var workType = this.closest('.work_type');
            if (workType) {
                workType.querySelector('.corp_wrap').classList.add('active');
            }
        });
    });
});

const subSwiper = new Swiper(".swiper_sub_banner", {
    // slidesPerGroup:3,
    slidesPerView: 1.2,
    spaceBetween: 10,
    speed: 1000,
    observer: true,
    observeParents: true,
    loopAdditionalSlides: 1,
    autoplay: false,
    scrollbar: {
        el: '.swiper-scrollbar',
        draggable: true,
    },
    breakpoints: {
        500: {
            centeredSlides: true,
            slidesPerView: 1.2,
            slidesPerGroup: 1,
        },
        1024: {
            centeredSlides: true,
            slidesPerView: 1.2,
            //   spaceBetween: 40,
        },
    },
});

