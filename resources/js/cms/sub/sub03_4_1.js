
const btn = document.querySelector('.btn_mini');

setTimeout(() => {
    const bar = document.querySelector('.bar');
    const txt = document.querySelector('.install_sts');
    const install_msg = document.querySelector('.install_msg');

    txt.classList.add('me')
    bar.value = '100'

    if (bar.value == '100') {
        setTimeout(() => {
            txt.innerText = "1/1";
            btn.style.display= "none";
            install_msg.style.display= "block"
        }, 500);
    }
}, 300);
