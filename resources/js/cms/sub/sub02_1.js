// sub02_1

const agreeOnline = (target) => {
  const agreementSections = document.querySelectorAll(".agree_cont");
    console.log(target)
  for (let i = 0; i < agreementSections.length; i++) {
    const radioBox = agreementSections[i].querySelector(".radio_box");
    const agreeRadio = radioBox.querySelector('input[type="radio"][id^="agree"]:checked');
    ;

    if(!agreeRadio){
      if (i == 0) {
        // 동의하지 않은 라디오 버튼이 있는 경우      
        alert("CMS이용약관 동의가 필요합니다.");
        window.scrollTo(0, agreementSections[i].offsetTop - 100);      
        return false;
      }else if(i == 1){
        alert("개인(신용)정보 수집 및 이용동의가 필요합니다.");
        window.scrollTo(0, agreementSections[i].offsetTop - 100);      
        return false;
      }else{
        alert("개인(신용)정보 제 3자 제공 동의가 필요합니다.");
        window.scrollTo(0, agreementSections[i].offsetTop - 100);      
        return false;
      }
    }
  }
  window.location.href = "./sub02_1_1_3";
};

const bizDiv = document.querySelectorAll("input[name=bizDiv]");
const bizDivCon1 = document.querySelector(".corpRegNum");
const bizDivCon2 = document.querySelector(".repManBirth");

bizDiv.forEach((inner,idx)=>{
  bizDiv[idx].addEventListener('click',function(e){
    console.log('clcick')
    if(idx==0){
      bizDivCon1.classList.remove('hide')
      bizDivCon2.classList.add('hide')
      document.querySelector('#repManBirth').setAttribute('disabled','disabled')
      document.querySelector('#corpRegNum').removeAttribute('disabled')
    }else{
      bizDivCon1.classList.add('hide')
      document.querySelector('#corpRegNum').setAttribute('disabled','disabled')
      document.querySelector('#repManBirth').removeAttribute('disabled')
      bizDivCon2.classList.remove('hide')
    }
  })
})
  

const radioAppl = document.querySelectorAll("input.radio_appl");
const wdTable = document.querySelector("#wdTable");
const dpTable = document.querySelector("#dpTable");

radioAppl.forEach((inner,idx)=>{
  radioAppl[idx].addEventListener('click',function(e){
    console.log('clcick')
    if(idx==0){      
      dpTable.querySelectorAll('input').forEach((inner)=>{
        inner.setAttribute('disabled','disabled')
      })
      wdTable.querySelectorAll('input').forEach((inner)=>{
        inner.removeAttribute('disabled')
      })
    }else{     
      dpTable.querySelectorAll('input').forEach((inner)=>{
        inner.removeAttribute('disabled')
      }) 
      wdTable.querySelectorAll('input').forEach((inner)=>{
        inner.setAttribute('disabled','disabled')
      })
    }
  })
})
  


