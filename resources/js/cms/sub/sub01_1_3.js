const popup02 = document.querySelector(".pop_cont_type02");
const popupButton = document.querySelectorAll(".btn_popup");
const tableZindex = document.querySelector(".table_cont.n1 .table_layout .line1 .act");


for(let i=0; i<popupButton.length; i++) {
    popupButton[i].addEventListener("click", function(){
        if(popup02.classList.contains("act")) {
            tableZindex.classList.add("z_inx")
        }else {
            tableZindex.classList.remove("z_inx")
        }
    });
}
