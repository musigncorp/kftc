const userName = sessionStorage.getItem("user_name");
const userNameClass = document.querySelectorAll(".user_name");
// const userName3 = document.querySelector('#user_name02');
const userNum = sessionStorage.getItem("user_num");
const userNumClass = document.querySelectorAll(".user_num");

// const userNum3 = document.querySelector('#user_num02');
const financialInstitution = sessionStorage.getItem("financial_institution");
const financialInstitutionClass = document.querySelectorAll(
  ".financial_institution"
);
// const financialInstitution3 = document.querySelector('#financial_institution02');
const accountHolder = sessionStorage.getItem("account_holder");
const accountHolderClass = document.querySelectorAll(".account_holder");
// const accountHolder3 = document.querySelector('#account_holder02');
const bankAccountNum = sessionStorage.getItem("bank_account_num");
const bankAccountNumClass = document.querySelectorAll(".bank_account_num");
// const bankAccountNum3 = document.querySelector('#bank_account_num02');
const accountHolderBirth = sessionStorage.getItem("account_holder_birth");
const accountHolderBirthClass = document.querySelectorAll(
  ".account_holder_birth"
);
// const accountHolderBirth3 = document.querySelector('#account_holder_birth02');
const accountContent = sessionStorage.getItem("account_content");
const accountContentClass = document.querySelectorAll(".account_content");

const confirm01 = document.querySelector("#confirm01");
const popUpBtn01 = document.querySelectorAll(
  ".sub01_2_5 .btn_popup_container.popup01"
);
const popUpBtn02 = document.querySelector(
  ".sub01_2_5 .btn_popup_container.popup02"
);

const confirm01Func = {
  case1: function (e) {
    if (
      popUpBtn01[0].classList.contains("on") ||
      popUpBtn01[1].classList.contains("on")
    ) {
      alert("출금대상을 추가해주세요");
      e.preventDefault();
      return false;
    }
  },
  case2: function (e) {
    if (popUpBtn02.classList.contains("on")) {
      alert("출금금액을 추가해주세요");
      e.preventDefault();
      return false;
    }
  },
};

if (userNameClass.length > 0) {
  userNameClass.forEach((inner) => {
    inner.value = userName;
  });
}

if (userNumClass.length > 0) {
  userNumClass.forEach((inner) => {
    inner.value = userNum;
  });
}

if (financialInstitutionClass.length > 0) {
  financialInstitutionClass.forEach((inner) => {
    inner.value = financialInstitution;
  });
}
if (accountHolderClass.length > 0) {
  accountHolderClass.forEach((inner) => {
    inner.value = accountHolder;
  });
}
if (bankAccountNumClass.length > 0) {
  bankAccountNumClass.forEach((inner) => {
    inner.value = bankAccountNum;
  });
}
if (accountHolderBirthClass.length > 0) {
  accountHolderBirthClass.forEach((inner) => {
    inner.value = accountHolderBirth;
  });
}
if (accountContentClass.length > 0) {
  accountContentClass.forEach((inner) => {
    inner.value = accountContent;
  });
}
// 납부자번호
// const directInput01 = document.querySelector("#user_num")

// if (directInput01) {
//   if (!sessionStorage.getItem("direct_input01")) {
//     let randomNum = generateRandomCode(10);
//     sessionStorage.setItem("direct_input01", randomNum);
//     directInput01.value = randomNum;
//   } else {
//     const directInput01Value = sessionStorage.getItem("direct_input01");
//     directInput01.value = directInput01Value;
//   }
// }

//====== 240122 새 코드 =======

// const elements = [
//   document.querySelector("#user_num"),
//   document.querySelector("#user_num02"),
//   document.querySelector("#user_num03"),
//   document.querySelector("#user_num04"),
//   document.querySelector("#user_num05"),
//   document.querySelector("#user_num06")
// ];

const elements = document.querySelectorAll(".user_num");

elements.forEach((element) => {
  if (element) {
    if (!sessionStorage.getItem("direct_input01")) {
      let randomNum = generateRandomCode(10);
      sessionStorage.setItem("direct_input01", randomNum);
      element.value = randomNum;
    } else {
      const elementValue = sessionStorage.getItem("direct_input01");
      element.value = elementValue;
    }
  }
});

const receiptAmount01 = sessionStorage.getItem("receipt_amount01");
const receiptCycleName01 = sessionStorage.getItem("receipt_cycle01");
const irregular = document.querySelectorAll(".irregular");
const withdrawalCostBtn02 = document.querySelector(".sub01_2_5 #confirm02");
const withdrawalCostBtn03 = document.querySelector(".sub01_2_5 #confirm03");
const withdrawalCost01 = document.querySelector("#withdrawal_cost01");
const withdrawalCost02 = document.querySelector("#withdrawal_cost02");
const withdrawalCost03 = document.querySelector("#withdrawal_cost03");
const withdrawalCost04 = document.querySelector("#withdrawal_cost04");
const withdrawalCost05 = document.querySelector("#withdrawal_cost05");
const withdrawalCost06 = document.querySelector("#withdrawal_cost06");
const directInput02 = document.querySelectorAll(".direct_input02");

if (receiptAmount01) {
  // 비정액일떄

  if (receiptCycleName01 == "비정기") {
    // 비정액 - 비정기
    irregular.forEach((inner) => {
      inner.classList.add("font_hide");
    });
    popUpBtn01.forEach((inner) => {
      inner.classList.add("on");
    });

    confirm01.addEventListener("click", confirm01Func.case1);

    // 비정기
    if (withdrawalCostBtn02) {
      withdrawalCostBtn02.addEventListener("click", function (e) {
        if (withdrawalCost01.value || withdrawalCost02.value) {
          sessionStorage.setItem("withdrawal_cost01", withdrawalCost01.value);
          sessionStorage.setItem("withdrawal_cost02", withdrawalCost02.value);
          let popClose = document.querySelector(".sub01_2_5 .pop_close.n1");
          popClose.click();
          popUpBtn01.forEach((inner) => {
            inner.classList.remove("on");
          });

          irregular.forEach((inner) => {
            inner.classList.remove("font_hide");
          });
          if (sessionStorage.getItem("withdrawal_cost01")) {
            directInput02.forEach((inner) => {
              inner.value = sessionStorage.getItem("withdrawal_cost01");
            });
          } else if (sessionStorage.getItem("withdrawal_cost02")) {
            directInput02.forEach((inner) => {
              inner.value = sessionStorage.getItem("withdrawal_cost02");
            });
          }
        } else {
          alert("금액을 입력해주세요.");
          e.preventDefault();
          return false;
        }
      });
    }
  } else {
    // 비정액 - 정기
    popUpBtn02.classList.add("on");

    confirm01.addEventListener("click", confirm01Func.case2);

    if (withdrawalCostBtn03) {
      // withdrawalCost03.addEventListener('keyup', function(e){
      //   if(e.keyCode != 8 && e.keyCode >= 48 && e.keyCode <= 57 || e.keyCode != 8 && e.keyCode >= 96 && e.keyCode <= 105){
      //     addCommas(e.target)
      //     withdrawalCost05.value = e.target.value +' 원'
      //     withdrawalCost03.value = withdrawalCost05.value
      //   }
      // })
      // withdrawalCost04.addEventListener('keyup', function(e){
      //   if(e.keyCode != 8 && e.keyCode >= 48 && e.keyCode <= 57 || e.keyCode != 8 && e.keyCode >= 96 && e.keyCode <= 105){
      //     addCommas(e.target)
      //     withdrawalCost06.value = e.target.value +' 원'
      //     withdrawalCost04.value = withdrawalCost06.value
      //   }
      // })
      withdrawalCostBtn03.addEventListener("click", function () {
        if (withdrawalCost04.value || withdrawalCost06.value) {
          sessionStorage.setItem("withdrawal_cost04", withdrawalCost04.value);
          sessionStorage.setItem("withdrawal_cost06", withdrawalCost06.value);
          let popClose = document.querySelector(".sub01_2_5 .pop_close.n2");
          popClose.click();
          console.log("here");
          popUpBtn02.classList.remove("on");
          irregular.forEach((inner) => {
            inner.classList.remove("font_hide");
          });
          // 청구일분, 합계
          //   if (!sessionStorage.getItem("direct_input02")) {
          //     // 정액이 아닐때
          //   }
          if (sessionStorage.getItem("withdrawal_cost04")) {
            directInput02.forEach((inner) => {
              inner.value = sessionStorage.getItem("withdrawal_cost04");
            });
          } else if (sessionStorage.getItem("withdrawal_cost06")) {
            directInput02.forEach((inner) => {
              inner.value = sessionStorage.getItem("withdrawal_cost06");
            });
          }
        } else {
          alert("금액을 입력해주세요.");
          withdrawalCost04.focus();
          return false;
        }
      });
    }
  }
} else {
  // 정액

  if (!sessionStorage.getItem("direct_input02")) {
    if (sessionStorage.getItem("withdrawal_cost01")) {
      directInput02.forEach((inner) => {
        inner.value = sessionStorage.getItem("withdrawal_cost01");
      });
    } else if (sessionStorage.getItem("withdrawal_cost02")) {
      directInput02.forEach((inner) => {
        inner.value = sessionStorage.getItem("withdrawal_cost02");
      });
    }
  } else {
    const directInput01Value = sessionStorage.getItem("direct_input02");
    directInput02.forEach((inner) => {
      inner.value = directInput01Value;
    });
  }
}

// 난수생성
function generateRandomCode(n) {
  let str = "";
  for (let i = 0; i < n; i++) {
    str += Math.floor(Math.random() * 10);
  }
  return str;
}

//출금금액 수정 팝업 청구일분 값 연동
withdrawalCost04.addEventListener("keyup", function () {
  const price = withdrawalCost04.value;
  withdrawalCost03.value = price;
});
withdrawalCost06.addEventListener("keyup", function () {
  const price = withdrawalCost06.value;
  withdrawalCost05.value = price;
});
