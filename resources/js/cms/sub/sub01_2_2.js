// 이전페이지로 넘어왔을때 값 삽입
const inputTextEnable = document.querySelectorAll("input[type=text]");
for (let i = 0; i < inputTextEnable.length; i++) {
  inputTextEnable[i].value = sessionStorage.getItem(inputTextEnable[i].id);
}

const inputRadioBox = document.querySelectorAll("input[type=radio]");
for (let i = 0; i < inputRadioBox.length; i++) {
  if (inputRadioBox[i].value == sessionStorage.getItem(inputRadioBox[i].id)) {
    inputRadioBox[i].setAttribute("checked", "checked");
  }
}
const inputAccountHolder = document.querySelector("input[id=account_holder]");
const accountHolderCheck01 = document.querySelector("#account_holder_check01");
const inputUserName = document.querySelector("input[id=user_name]");

accountHolderCheck01.addEventListener("change", function () {
  if (this.checked) {
    inputAccountHolder.value = inputUserName.value;    
  } else {
    inputAccountHolder.value = ''
  }
});

// submit
const inputCycle = document.querySelectorAll("input[name=receipt_cycle]");

function saveToSessionStorage() {
  window.sessionStorage.clear();
  // 구분
  let title;
  const inputUser = document.querySelectorAll("input[name=user]");
  for (let idx = 0; idx < inputUser.length; idx++) {
    const nameArray = [];
    if (idx > 0 && !inputUser[idx].checked && !inputUser[idx - 1].checked) {
      title = inputUser[idx].closest(".radio_box").previousElementSibling;
      alert(title.innerHTML + "을(를) 입력해주세요");
      window.scrollTo(0, title.offsetTop - 200);
      return false;
    }
  }
  // 고객명

  if (inputUserName.value == "") {
    alert("고객명을(를) 입력해주세요");
    inputUserName.focus();
    return false;
  }
  // 주민등록번호, 사업자등록번호
  const inputUserNum = document.querySelector("input[id=user_num]");
  let lengthCheck = inputUserNum.getAttribute("minlength");
  if (inputUserNum.value == "" || inputUserNum.value.length < lengthCheck) {
    title = inputUserNum.closest(".input_box").previousElementSibling;
    alert(title.innerHTML + "을(를) 입력해주세요");
    inputUserNum.focus();
    return false;
  }
  // 납부자번호
  const inputPayNumber = document.querySelectorAll("input[name=payer_number]");
  for (let idx = 0; idx < inputPayNumber.length; idx++) {
    const nameArray = [];
    if (
      idx > 0 &&
      !inputPayNumber[idx].checked &&
      !inputPayNumber[idx - 1].checked
    ) {
      title =
        inputPayNumber[idx].closest(".radio_box").previousElementSibling
        .childNodes[0].textContent;
      title = title.replace(/\s/g, "");
      // console.log(title, "2");
      alert(title + "을(를) 입력해주세요");
      window.scrollTo(
        0,
        inputPayNumber[idx].closest(".radio_box").previousElementSibling
        .offsetTop - 100
      );
      return false;
    }
  }
  // 직접입력, 정액, 횟수제한
  const inputDirect = document.querySelectorAll("input.direct_input:enabled");
  for (let idx = 0; idx < inputDirect.length; idx++) {
    if (inputDirect[idx].value == "") {
      title =
        inputDirect[idx].closest("li").children[0].childNodes[0].textContent;
      title = title.replace(/\s/g, "");
      alert(title + "을(를) 입력해주세요");
      inputDirect[idx].focus();
      break;
    }
  }
  // 수납금액
  const inputReciptAmout = document.querySelectorAll(
    "input[name=receipt_amount]"
  );
  for (let idx = 0; idx < inputReciptAmout.length; idx++) {
    if (
      idx > 0 &&
      !inputReciptAmout[idx].checked &&
      !inputReciptAmout[idx - 1].checked
    ) {
      title =
        inputReciptAmout[idx].closest(".radio_box").previousElementSibling;
      alert(title.innerHTML + "을(를) 입력해주세요");
      window.scrollTo(0, title.offsetTop - 200);
      return false;
    }
  }
  // 수납일
  const inputDate = document.querySelectorAll("input[name=receipt_date]");
  const receiptDate02 = document.querySelector("#receipt_date02");
  for (let idx = 0; idx < inputDate.length; idx++) {
    if (idx > 0 && !inputDate[idx].checked && !inputDate[idx - 1].checked) {
      title = inputDate[idx].closest(".radio_box").previousElementSibling;
      alert(title.innerHTML + "을(를) 입력해주세요");
      window.scrollTo(0, title.offsetTop - 200);
      return false;
    } else if (inputDate[0].checked && !receiptDate02.value) {
      title = inputDate[0].closest(".radio_box").previousElementSibling;
      alert(title.innerHTML + "을(를) 입력해주세요2");
      window.scrollTo(0, title.offsetTop - 200);
      return false;
    }
  }
  // 수납횟수
  const inputReciptNum = document.querySelectorAll("input[name=receipt_num]");
  for (let idx = 0; idx < inputReciptNum.length; idx++) {
    if (
      idx > 0 &&
      !inputReciptNum[idx].checked &&
      !inputReciptNum[idx - 1].checked
    ) {
      title = inputReciptNum[idx].closest(".radio_box").previousElementSibling;
      alert(title.innerHTML + "을(를) 입력해주세요");
      window.scrollTo(0, title.offsetTop - 200);
      return false;
    }
  }
  // 수납주기

  const receiptCycle02 = document.querySelector("#receipt_cycle03");
  for (let idx = 0; idx < inputCycle.length; idx++) {
    if (idx > 0 && !inputCycle[idx].checked && !inputCycle[idx - 1].checked) {
      title = inputCycle[idx].closest(".radio_box").previousElementSibling;
      alert(title.innerHTML + "을(를) 입력해주세요");
      window.scrollTo(0, title.offsetTop - 200);
      return false;
    } else if (inputCycle[1].checked && !receiptCycle02.value) {
      title = inputCycle[0].closest(".radio_box").previousElementSibling;
      alert(title.innerHTML + "을(를) 입력해주세요2");
      window.scrollTo(0, title.offsetTop - 200);
      return false;
    }
  }
  // 수납시작월
  const inputStartMonth = document.querySelectorAll(
    "input[name=receipt_start_month]"
  );
  // const receiptCycle02 = document.querySelector('#receipt_cycle03')
  for (let idx = 0; idx < inputStartMonth.length; idx++) {
    if (!inputStartMonth[idx].value) {
      title = inputStartMonth[idx].closest(".radio_box").previousElementSibling;
      alert(title.innerHTML + "을(를) 입력해주세요");
      window.scrollTo(0, title.offsetTop - 200);
      return false;
    }
  }
  // 금융기관
  const inputFin = document.querySelector("input[id=financial_institution]");
  if (inputFin.value == "") {
    alert("금융기관을(를) 입력해주세요");
    inputFin.focus();
    return false;
  }
  // 계좌번호
  const inputBank = document.querySelector("input[id=bank_account_num]");
  if (inputBank.value == "") {
    alert("계좌번호을(를) 입력해주세요");
    inputBank.focus();
    return false;
  }
  // 예금주

  if (inputAccountHolder.value == "") {
    alert("예금주을(를) 입력해주세요");
    inputAccountHolder.focus();
    return false;
  }

  // 예금주 생년월일(사업자번호)
  const inputAccountHolder2 = document.querySelector(
    "input[id=account_holder_birth]"
  );
  if (inputAccountHolder2.value == "") {
    alert("예금주 생년월일(사업자번호)을(를) 입력해주세요");
    inputAccountHolder2.focus();
    return false;
  }
  // 통장기재내용
  const inputAccountContent = document.querySelector(
    "input[id=account_content]"
  );
  if (inputAccountContent.value == "") {
    alert("통장기재내용을(를) 입력해주세요");
    inputAccountContent.focus();
    return false;
  }

  const inputTextEnable = document.querySelectorAll("input[type=text]:enabled");
  for (let i = 0; i < inputTextEnable.length; i++) {
    sessionStorage.setItem(inputTextEnable[i].id, inputTextEnable[i].value);
  }

  const inputRadioBox = document.querySelectorAll("input[type=radio]:checked");
  for (let i = 0; i < inputRadioBox.length; i++) {
    sessionStorage.setItem(inputRadioBox[i].id, inputRadioBox[i].value);
  }

  // 다른 페이지로 이동
  window.location.href = "./sub01_2_3";
}

// 수납정보 프로세스

// 납부자번호
const directInput01 = document.querySelector("#direct_input01");
const payerMumber = document.querySelectorAll("input[name=payer_number]");
payerMumber.forEach((inner, idx) => {
  inner.addEventListener("click", function () {
    if (idx == 0) {
      directInput01.setAttribute("disabled", "disabled");
    } else {
      directInput01.removeAttribute("disabled");
    }
  });
});
// 수납 금액
const directInput02 = document.querySelector("#direct_input02");
const receiptAmount = document.querySelectorAll("input[name=receipt_amount]");

receiptAmount.forEach((inner, idx) => {
  inner.addEventListener("click", function () {
    if (idx == 0) {
      // 비정액
      directInput02.setAttribute("disabled", "disabled");
    } else {
      // 정액
      directInput02.removeAttribute("disabled");
    }
  });
});
// 수납일
const receiptDate = document.querySelectorAll("input[name=receipt_date]");
const receiptDateSelect = document.querySelector("#receipt_date_select");
const receiptDate02 = document.querySelector("#receipt_date02");

receiptDate.forEach((inner, idx) => {
  inner.addEventListener("click", function () {
    if (idx == 0) {
      // 지정일
      receiptDateSelect.classList.remove("disabled");
    } else {
      // 매월 마지막 영업일
      receiptDateSelect.children[0].innerHTML = "날짜";
      receiptDate02.value = "";
      receiptDateSelect.classList.add("disabled");
    }
  });
});
// 수납 횟수
const directInput03 = document.querySelector("#direct_input03");
const receiptNum = document.querySelectorAll("input[name=receipt_num]");
const receiptEndMonth = document.querySelectorAll(".receipt_end_month");
const receiptCycleSelect04 = document.querySelector("#receipt_cycle_select04");
const receiptCycleSelect05 = document.querySelector("#receipt_cycle_select05");

receiptNum.forEach((inner, idx) => {
  inner.addEventListener("click", function () {
    if (idx == 0) {
      // 기한없음
      directInput03.setAttribute("disabled", "disabled");
      directInput03.value = "";
      receiptCycleSelect04.children[0].innerHTML = "년도";
      receiptCycleSelect05.children[0].innerHTML = "월";
      receiptEndMonth.forEach((innrer) => {
        innrer.classList.add("disabled");
      });
    } else {
      // 횟수제한
      directInput03.removeAttribute("disabled");
      receiptEndMonth.forEach((innrer) => {
        innrer.classList.remove("disabled");
      });
    }
  });
});
// 수납 주기
let receiptCycleIrregularity = false;
// const directInput03 = document.querySelector('#direct_input03')
const receiptCycle = document.querySelectorAll("input[name=receipt_cycle]");
const receiptCycleSelect = document.querySelector("#receipt_cycle_select");
receiptCycle.forEach((inner, idx) => {
  inner.addEventListener("click", function () {
    receiptCycleSelect.classList.add("disabled");
    if (idx == 0 && !receiptNum[0].checked) {
      // sessionStorage.setItem('receipt_cycle', inner.value);
      receiptCycleSelect.querySelector('input').value = "";
      receiptCycleSelect.querySelector('.fake_selected').innerHTML = "주기";
      receiptEndMonth.forEach((innrer) => {
        innrer.classList.remove("disabled");
      });
      receiptCycleIrregularity = true;
    } else {
      // 정기
      // sessionStorage.setItem('receipt_cycle', inner.value);
      receiptCycleSelect.classList.remove("disabled");
      //here
      receiptEndMonth.forEach((innrer) => {
        innrer.classList.add("disabled");
      });
    }
  });
});
// 수납 주기 셀렉트 박스 컨텐츠 클릭시
const receiptCycleLi = document.querySelectorAll(
  "#receipt_cycle_select .select_li"
);
const receiptCycle2 = document.querySelector("#receipt_cycle_select02");
const receiptCycleLi2 = document.querySelectorAll(
  "#receipt_cycle_select02 .select_li"
);
const receiptCycle3 = document.querySelector("#receipt_cycle_select03");
const receiptCycleLi3 = document.querySelectorAll(
  "#receipt_cycle_select03 .select_li"
);
const receiptCycleLi4 = document.querySelectorAll(
  "#receipt_cycle_select04 .select_li"
);
const receiptCycle4 = document.querySelector(
  "#receipt_cycle_select04 .select_box_inner"
);
const receiptCycleLi5 = document.querySelectorAll(
  "#receipt_cycle_select05 .select_li"
);
/** 수납주기 - 정기  */
let receiptCycleRange;
/** 수납시작년도 */
let receiptStartYear;
/** 수납시작월 */
let receiptStartMonth;
/** 수납종료년도 */
let receiptEndYear;
/** 수납종료월 */
let receiptEndMonth2;
/** 수납횟수 */
let count;
let receiptCycleLi4Year;
const nowDate = new Date(); // 현재 날짜 및 시간
const nowYear = nowDate.getFullYear() - 2023; // 연도
const nowMonth = nowDate.getMonth(); // 월

directInput03.addEventListener("keyup", function (e) {
  if (e.target.value == "0") {
    alert("1회 이상 입력해주세요.");
    e.target.value = "";
    directInput03.focus();
    return false;
  } else if (e.target.value == "1") {
    // console.log(inputCycle,'inputCycle')
    inputCycle.forEach((inner) => {
      inner.setAttribute("disabled", "disabled");
    });
    // 231229
    receiptCycleSelect04.classList.add('disabled')
    receiptCycleSelect05.classList.add('disabled')

  } else {
    inputCycle.forEach((inner) => {
      inner.removeAttribute("disabled");
    });
  }
  count = directInput03.value;

  receiptFucn(
    receiptCycleRange,
    receiptStartYear,
    receiptStartMonth,
    count,
    receiptEndYear
  );
});
receiptCycleLi.forEach((inner, idx) => {
  inner.addEventListener("click", function () {
    receiptCycleRange = Number(inner.dataset.receiptCycle);
    receiptFucn(
      receiptCycleRange,
      receiptStartYear,
      receiptStartMonth,
      count,
      receiptEndYear
    );
  });
});
receiptCycle2.addEventListener("click", function () {
  receiptCycleLi2.forEach((inner, idx) => {
    if (inner.dataset.year < nowYear) {
      inner.classList.add("disabled");
    }
  });
});
receiptCycleLi2.forEach((inner, idx) => {
  inner.addEventListener("click", function () {
    if (inner.classList.contains("disabled")) {
      alert("수납시작월을 확인해주세요.");
    } else {
      receiptStartYear = Number(inner.dataset.year);
      receiptFucn(
        receiptCycleRange,
        receiptStartYear,
        receiptStartMonth,
        count,
        receiptEndYear
      );
    }
  });
});

receiptCycle3.addEventListener("click", function () {
  if (receiptStartYear != "undefined" && receiptStartYear <= nowYear) {
    receiptCycleLi3.forEach((inner, idx) => {
      if (inner.dataset.month < nowMonth) {
        inner.classList.add("disabled");
      }
    });
  }
});
receiptCycleLi3.forEach((inner, idx) => {
  inner.addEventListener("click", function () {
    if (inner.classList.contains("disabled")) {
      alert("수납시작월을 확인해주세요.");
    } else {
      receiptStartMonth = Number(inner.dataset.month);
      receiptFucn(
        receiptCycleRange,
        receiptStartYear,
        receiptStartMonth,
        count,
        receiptEndYear
      );
    }
  });
});

receiptCycleLi4.forEach((inner, idx) => {
  inner.addEventListener("click", function (e) {
    if (receiptCycleIrregularity && e.target.dataset.year < receiptStartYear) {
      alert("수납 종료월을 확인해주세요.");
      e.preventDefault();
      return false;
    } else {
      receiptEndYear = e.target.dataset.year;
    }
    receiptFucn(
      receiptCycleRange,
      receiptStartYear,
      receiptStartMonth,
      count,
      receiptEndYear
    );
  });
});
receiptCycleLi5.forEach((inner, idx) => {
  inner.addEventListener("click", function (e) {
    // console.log(receiptCycleIrregularity, "receiptCycleIrregularity");
    if (
      receiptCycleIrregularity &&
      e.target.dataset.month < receiptStartMonth
    ) {
      alert("수납 종료월을 확인해주세요.");
      e.preventDefault();
      return false;
    } else {
      receiptEndMonth2 = e.target.dataset.month;
    }
    receiptFucn(
      receiptCycleRange,
      receiptStartYear,
      receiptStartMonth,
      count,
      receiptEndYear
    );
    // console.log(inner.dataset.month,'receiptStartMonth')
    // receiptStartMonth = Number(inner.dataset.month)
    //   receiptFucn(receiptCycleRange,receiptStartYear,receiptStartMonth, count, receiptEndYear, receiptEndMonth2)
  });
});

function receiptFucn(receiptCycleRange, receiptStartYear, receiptStartMonth, count, receiptEndYear) {
  // console.log("receiptCycleRange", receiptCycleRange);
  // console.log("receiptStartYear", receiptStartYear);
  // console.log("receiptStartMonth", receiptStartMonth);
  // console.log("count", count);
  // console.log("receiptEndYear", receiptEndYear);



  if (directInput03.hasAttributes("disabled") && directInput03.value == "" && count) {
    alert("횟수를 입력해주세요.");
    directInput03.focus();
    return false;
  }

  receiptCycleLi3.forEach((inner, idx) => {
    if (receiptStartYear > nowYear) {
      inner.classList.remove("disabled");
    } else if (receiptStartYear == nowYear) {
      if (inner.dataset.month > nowMonth) {
        inner.classList.remove("disabled");
      }
    }
  });

  receiptCycleLi4.forEach((inner, idx) => {
    // 수납종료년도가 시작년도 보다 작을때
    if (receiptCycleLi4[idx].dataset.year < receiptStartYear) {
      inner.classList.add("disabled");
    }
  });
  if (receiptEndYear != "undefined" && count != 1) {
    receiptCycleLi5.forEach((inner, idx) => {
      if (receiptStartYear == receiptEndYear && receiptCycleLi5[idx].dataset.month < receiptStartMonth) {
        inner.classList.add("disabled");
      }
    });
  }

  if (receiptCycleRange && receiptStartYear >= 0 && receiptStartMonth >= 0 && count && receiptCycle[1].checked) {
    receiptCycleRange = receiptCycleRange * (count - 1);

    console.log(receiptCycleRange, "receiptCycleRangeTTTT");
    let year = Math.floor((receiptCycleRange + receiptStartMonth) / 12);
    let month = (receiptCycleRange + receiptStartMonth) % 12;

    // console.log(year, "year");
    // console.log(month, "month");
    if (year == 0) {
      receiptCycleLi4[receiptStartYear].click();
      receiptCycleLi5[receiptCycleRange + receiptStartMonth].click();

    } else if (year > 0 && year < 55) {
      // receiptStartMonth + month 가 12가 넘는지 여부
      if (month == 0) {
        console.log(receiptStartYear,'receiptStartYear')
        console.log(year,'year')
        console.log(receiptStartMonth,'receiptStartMonth')
        receiptCycleLi4[receiptStartYear + year].click();
        receiptCycleLi5[receiptStartMonth].click();
      } else {
        console.log(receiptStartYear,'receiptStartYear')
        console.log(year,'year')
        console.log(month,'month')
        receiptCycleLi4[receiptStartYear + year].click();
        receiptCycleLi5[month].click();
      }

    } else {
      alert("수납시작 년,월을 확인해주세요3");
    }
  }



  if (count == 1 && receiptCycleRange == undefined) {

    // console.log('횟수제한1일때)')
    receiptCycleLi4[receiptStartYear].click();
    receiptCycleLi5[receiptStartMonth].click();
  }
}

// 주민등록번호, 사업자번호

const user = document.querySelectorAll("input[name=user]");
user.forEach((inner, idx) => {
  inner.addEventListener("click", function (e) {
    let userNumSpan = document.querySelector("label[for=user_num]");
    if (e.target.id == "user02") {
      userNumSpan.children[0].innerHTML = "사업자 등록번호(10자리)";
      userNumSpan.children[1].children[0].setAttribute("minlength", "10");
      userNumSpan.children[1].children[0].setAttribute("maxlength", "10");
    } else {
      userNumSpan.children[0].innerHTML = "주민등록번호 앞자리(6자리)";
      userNumSpan.children[1].children[0].setAttribute("minlength", "6");
      userNumSpan.children[1].children[0].setAttribute("maxlength", "6");
    }
  });
});