// window.sessionStorage.clear()
function saveToSessionStorage() {
    
    window.sessionStorage.clear()
    
    // 인풋입력
    const inputText = document.querySelectorAll('input[type=text]:enabled');
    
    for(let i = 0; i < inputText.length ; i++){
        sessionStorage.setItem(inputText[i].id, inputText[i].value);
    }
    
    
    
    // 다른 페이지로 이동
    window.location.href = './sub01_2_3';
}
// 납부자번호
const directInput01 = document.querySelector('#direct_input01')
const payerMumber = document.querySelectorAll('input[name=payer_number]')
payerMumber.forEach((inner,idx)=>{
    inner.addEventListener('click',function(){
        if(idx==0){
            directInput01.setAttribute('disabled','disabled')
        }else{
            directInput01.removeAttribute('disabled')
        }
    })
})
// 수납 금액
const directInput02 = document.querySelector('#direct_input02')
const receiptAmount = document.querySelectorAll('input[name=receipt_amount]')
receiptAmount.forEach((inner,idx)=>{
    inner.addEventListener('click',function(){
        if(idx==0){
            directInput02.setAttribute('disabled','disabled')
        }else{
            directInput02.removeAttribute('disabled')
        }
    })
})
// 수납일
const receiptDate = document.querySelectorAll('input[name=receipt_date]')
const receiptDateSelect = document.querySelector('#receipt_date_select')
const receiptDate02 = document.querySelector('#receipt_date02')

receiptDate.forEach((inner,idx)=>{
    inner.addEventListener('click',function(){
        if(idx==0){
            // 지정일
            receiptDateSelect.classList.remove('disabled')
        }else{
            // 매월 마지막 영업일
            receiptDateSelect.children[0].innerHTML ='날짜'
            receiptDate02.value =''
            receiptDateSelect.classList.add('disabled')
        }
    })
})
// 수납 횟수
const directInput03 = document.querySelector('#direct_input03')
const receiptNum = document.querySelectorAll('input[name=receipt_num]')
const receiptEndMonth = document.querySelectorAll('.receipt_end_month')
receiptNum.forEach((inner,idx)=>{
    inner.addEventListener('click',function(){
        if(idx==0){
            // 기한없음
            directInput03.setAttribute('disabled','disabled')
            receiptEndMonth.forEach((innrer)=>{
                innrer.classList.remove('disabled')
            })
        }else{
            // 횟수제한
            directInput03.removeAttribute('disabled')
            receiptEndMonth.forEach((innrer)=>{
                innrer.classList.add('disabled')
            })
        }
    })
})
// 수납 주기
// const directInput03 = document.querySelector('#direct_input03')
const receiptCycle = document.querySelectorAll('input[name=receipt_cycle]')
const receiptCycleSelect= document.querySelector('#receipt_cycle_select')
receiptCycle.forEach((inner,idx)=>{
    inner.addEventListener('click',function(){
        if(idx==0){
            sessionStorage.setItem('receipt_cycle', inner.value);
            receiptCycleSelect.classList.add('disabled')
        }else{
            sessionStorage.setItem('receipt_cycle', inner.value);
            receiptCycleSelect.classList.remove('disabled')
        }
    })
})