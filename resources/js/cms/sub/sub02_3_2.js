const subSwiper1 = new Swiper(".swiper_sub_banner", {
    // slidesPerGroup:3,
    slidesPerView: 1.2,
    spaceBetween: 15,
    speed: 1000,
    observer: true,
    observeParents: true,
    loopAdditionalSlides : 1,
    autoplay: false,
    scrollbar: {
        el: '.swiper-scrollbar',
        draggable: true,
    },
    breakpoints: {
        500: {
            centeredSlides: true,
            slidesPerView: 1.2,
            slidesPerGroup:1,
            spaceBetween: 10,
        },
        1024: {
            centeredSlides: true,
            slidesPerView: 1.2,
            slidesPerGroup:20,
        //   spaceBetween: 40,
        },
    },
});