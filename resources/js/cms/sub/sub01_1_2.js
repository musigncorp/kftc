
 // window.sessionStorage.clear()


 const customerNum = sessionStorage.getItem('customer_num');
 const customerNum2 = document.querySelector('#customer_num');
 const withdrawalLimit = sessionStorage.getItem('withdrawal_limit');
 const withdrawalLimit2 = document.querySelector('#withdrawal_limit');
 const requestCostNum = sessionStorage.getItem('request_cost_num');
 const requestCostNum2 = document.querySelector('#request_cost_num');
 const withdrawalRate = sessionStorage.getItem('withdrawal_rate');
 const withdrawalRate2 = document.querySelector('#withdrawal_rate');
 const requestNum = sessionStorage.getItem('request_num');
 const requestNum2 = document.querySelector('#request_num');
 const withdrawalNum = sessionStorage.getItem('withdrawal_num');
 const withdrawalNum2 = document.querySelector('#withdrawal_num');
 const insuranceRate = sessionStorage.getItem('insurance_rate');
 const insuranceRate2 = document.querySelector('#insurance_rate');
 const access = sessionStorage.getItem('access');
 const access2 = document.querySelector('#access');
 const program = sessionStorage.getItem('program');
 const program2 = document.querySelector('#program');
 let cost
 
 if(customerNum && customerNum2){
     customerNum2.value = customerNum 
 }
 if(withdrawalLimit && withdrawalLimit2){    
     cost = Number(withdrawalLimit).toLocaleString('ko-KR')
     withdrawalLimit2.value = cost 
 }
 if(requestCostNum && requestCostNum2){
     requestCostNum2.value = requestCostNum 
 }
 if(withdrawalRate && withdrawalRate2){
     withdrawalRate2.value = withdrawalRate   
 }
 if(requestNum && requestNum2){
     cost = Number(requestNum).toLocaleString('ko-KR')
     requestNum2.value = cost 
 }   
 if(withdrawalNum && withdrawalNum2){    
     cost = Number(withdrawalNum).toLocaleString('ko-KR')
     withdrawalNum2.value = cost 
 }   
 if(insuranceRate && insuranceRate2){
     insuranceRate2.value = insuranceRate 
 }   
 if(access && access2){
     access2.value = access
 }   
 if(program && program2){
     program2.value = program
 }   

 const inputText = document.querySelectorAll('input[type=text]:enabled');

function saveToSessionStorage(n) {        
  // 인풋입력  
  for(let i = 0; i < inputText.length ; i++){
    if(inputText[i].id == 'withdrawal_rate'){
      inputText[i].value = inputText[i].value
    }else if(inputText[i].id == 'insurance_rate'){
      inputText[i].value = inputText[i].value
    }else{
      inputText[i].value = inputText[i].value.replace(/[,|.]/g, '');

    }
      sessionStorage.setItem(inputText[i].id, inputText[i].value);
  }
  const inputRadio = document.querySelector('input[type=radio][name=access]:checked');
  const inputRadio2 = document.querySelector('input[type=radio][name=program]:checked');
  if(n && n==6){
    if(inputRadio && inputRadio2){    
      sessionStorage.setItem(inputRadio.name, inputRadio.value);
      sessionStorage.setItem(inputRadio2.name, inputRadio2.value);
        // 다른 페이지로 이동
      window.location.href = './sub01_1_2_'+n;
    }else{
      alert('선택항목을 체크해주세요')
    }

  }else{
      // 다른 페이지로 이동
      window.location.href = './sub01_1_2_'+n;
  }
}
const withdrawalLimit3 = document.querySelector('#withdrawal_limit2')
if(withdrawalLimit3){
  cost = sessionStorage.getItem('withdrawal_limit');
  withdrawalLimit3.value = Number(cost).toLocaleString('ko-KR')
}


/** 천단위, 도입비용 유효성 */ 
const allInputText = document.querySelectorAll('input[type=text]')
let blurBeforeValue
const NumTransfer = {
  change : function(target){    
    allInputText.forEach((inner,idx) =>{
      let cost
      if(allInputText[idx].id == 'withdrawal_rate'){
        cost = Number(allInputText[idx].value)    
      }else if(allInputText[idx].id == 'insurance_rate'){
        cost = Number(allInputText[idx].value)    
      }else{
        cost = Number(allInputText[idx].value.replace(/[,|.]/g, ''))           

      }
      // allInputText[idx].dataset.value = allInputText[idx].value
      if(cost >0){        
        if(allInputText[idx].id == 'withdrawal_limit' && cost < 5000000 ){          
          allInputText[idx].parentElement.parentElement.nextElementSibling.classList.add('show')
          allInputText[idx].focus()
          allInputText[idx].value = ''
        }else if(allInputText[idx].id == 'request_num' && cost < 20 ){
          allInputText[idx].parentElement.parentElement.nextElementSibling.classList.add('show')
          allInputText[idx].focus()
          allInputText[idx].value = ''
        }else if(allInputText[idx].id == 'withdrawal_num' && cost < 120 ){
          allInputText[idx].parentElement.parentElement.nextElementSibling.classList.add('show')
          allInputText[idx].focus()
          allInputText[idx].value = ''
        }else if(allInputText[idx].id == 'insurance_rate'){
          allInputText[idx].value = cost
        }else{
          allInputText[idx].value = cost.toLocaleString('ko-KR')
          if(allInputText[idx].parentElement.parentElement.nextElementSibling){
            allInputText[idx].parentElement.parentElement.nextElementSibling.classList.remove('show')
          }
        }
        
      }

    })
  },
}


if (allInputText.length > 0) {
  window.addEventListener("change", NumTransfer.change);
}

/** onblur */
function blur(taget){
  // console.log(taget.value,'taget.value')
}


/** 리셋예외 */
function resetExcept() {
  for (let i = 0; i < inputText.length; i++) {
    sessionStorage.setItem(inputText[i].id, '');
    inputText[i].value = ''
  }
}