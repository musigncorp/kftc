
function saveToSessionStorage() {
 
  
    const inputRadio = document.querySelector("input[type=radio][name=withdrawal_date]:checked");      
    const inputRadio2 = document.querySelector("input[type=radio][name=target]:checked");      
    const inputRadio3 = document.querySelector("input[type=radio][name=type]:checked");  
    if(!inputRadio){
      alert('출금청구 내용을 체크해주세요')
      return false;
    }
    if(!inputRadio2){
      alert('출금청구 대상을 체크해주세요')
      return false;
    }
    if(!inputRadio3){
      alert('출금방식 구분을 체크해주세요')
      return false;
    }
    sessionStorage.setItem(inputRadio.name, inputRadio.value);    
    // 다른 페이지로 이동
    window.location.href = "./sub01_2_5";
  
}
