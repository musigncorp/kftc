

const inputInfoCopy = document.querySelectorAll('input[name=info_copy]')

inputInfoCopy.forEach((inner,idx)=>{
    inner.addEventListener('click',function(){
        let id_ = this.id
        let num = id_.substr(9,2)
        // console.log(num,'num')

        let inputIdName = document.querySelector('input[id=name01]')
        let inputIdPhone = document.querySelector('input[id=phone_num01]')
        let inputIdPhone2 = document.querySelector('input[id=tel_num01]')
        let inputIdEmail = document.querySelector('input[id=email01]')
        let inputIdPart = document.querySelector('input[id=part_name01]')
        let inputIdPosition = document.querySelector('input[id=position01]')
        let inputIdFax = document.querySelector('input[id=fax_num01]')
        
        let inputName = document.querySelector('input[id=name'+ num+']')
        let inputPhone = document.querySelector('input[id=phone_num'+ num+']')
        let inputPhone2 = document.querySelector('input[id=tel_num'+ num+']')
        let inputEmail = document.querySelector('input[id=email'+ num+']')
        let inputPart = document.querySelector('input[id=part_name'+ num+']')
        let inputPosition = document.querySelector('input[id=position'+ num+']')
        let inputFax = document.querySelector('input[id=fax_num'+ num+']')
        
        // console.log(inputIdName,'inputIdName')
        // console.log(inputName,'inputName')

        function handleValidation(input, alertMessage, focusElement) {
            if (input.value == '') {
                alert(alertMessage);
                focusElement.focus();
                inner.checked = false; // Uncheck the radio input
                return false;
            }
            return true;
        }
        
        
        // console.log(inputName)
        if (!handleValidation(inputIdName, "성명을 입력해주세요.", inputIdName)) {
            return false;
        }
        if (!handleValidation(inputIdPhone, "휴대전화번호을 입력해주세요.", inputIdPhone)) {
            return false;
        }
        if (!handleValidation(inputIdPhone2, "전화번호을 입력해주세요.", inputIdPhone2)) {
            return false;
        }
        if (!handleValidation(inputIdEmail, "이메일을 입력해주세요.", inputIdEmail)) {
            return false;
        }
        if (!handleValidation(inputIdPart, "담당부서을 입력해주세요.", inputIdPart)) {
            return false;
        }
        if (!handleValidation(inputIdPosition, "직위을 입력해주세요.", inputIdPosition)) {
            return false;
        }
        if(inputIdFax){
            if (!handleValidation(inputIdFax, "팩스번호을 입력해주세요.", inputIdFax)) {
                return false;
            }
            inputFax.value = inputIdFax.value
        }
        
        inputName.value = inputIdName.value
        inputPhone.value = inputIdPhone.value
        inputPhone2.value = inputIdPhone2.value
        inputEmail.value = inputIdEmail.value
        inputPart.value = inputIdPart.value
        inputPosition.value = inputIdPosition.value


        
        

    })
})












