// 더보기 버튼 (컨텐츠 10개 제한)
// document.addEventListener('DOMContentLoaded', function() {
//     if(true) {
//         const listItems = document.querySelectorAll('.sec_cont_wrap.n2 li');
        
//         const initialItemsToShow = 10;
//         for (let i = 0; i < listItems.length; i++) {
//             if (i < initialItemsToShow) {
//             listItems[i].style.display = 'block';
//             } else {
//             listItems[i].style.display = 'none';
//             }
//         }

        // const moreButton = document.querySelector('.btn_box.btn_more');
        // moreButton.addEventListener('click', function() {
        //     for (let i = initialItemsToShow; i < listItems.length; i++) {
        //     listItems[i].style.display = 'block';
        //     }
        //     moreButton.style.display = 'none';
        // });

        // if (listItems.length <= initialItemsToShow) {
        //     moreButton.style.display = 'none';
        // } else {
        //     moreButton.style.display = 'flex';
        // }
//     }
// });

// 상단 배너
const subSwiper4 = new Swiper(".swiper_sub_banner", {
    // slidesPerGroup:3,
    slidesPerView: 1,
    spaceBetween: 15,
    speed: 1000,
    observer: true,
    observeParents: true,
    loopAdditionalSlides : 1,
    autoplay: false,
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
    scrollbar: {
        el: '.swiper-scrollbar',
        draggable: true,
    },
    breakpoints: {
        320: {
            centeredSlides: true,
            slidesPerView: 1.2,
            slidesPerGroup:1,
            spaceBetween: 15,
        },
        500: {
            centeredSlides: true,
            slidesPerView: 1.2,
            slidesPerGroup:1,
            spaceBetween: 20,
        },
        768: {
            centeredSlides: false,
            slidesPerView: 3,
            slidesPerGroup:3,
        //   spaceBetween: 40,
        },
    },
});