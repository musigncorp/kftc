// main slideBanner 
const mainPopSwiper1 = new Swiper(".popup_main_slider", {
    autoplay: true,
    slidesPerView: 1,
    // direction: 'vertical',
    loop: true,
    pagination: false,
    navigation: false,
    pauseOnMouseEnter: true,
    navigation: {
        nextEl: ".popup_main_slider .swiper-button-next",
        prevEl: ".popup_main_slider .swiper-button-prev",
    },
    
});

const mainSwiper1 = new Swiper(".rolling_banner01", {
    speed: 1500,
    loop: true,
    observer: true,
    observeParents: true,
    loopAdditionalSlides : 1,
    slidesPerView: 1,
    effect:'fade',
    spaceBetween: 20,
    autoplay: {
       delay: 4000
    },
    pagination: {
        el: ".section01 .left.swiper_banner_wrap .swiper-pagination",
        clickable: true
    },
    
});

const slidePause01 = document.querySelector('.section01 .left .slide_pause')
const slidePlay01 = document.querySelector('.section01 .left .slide_play')
const sliderControl = {
    pause: function(e){       
        e.currentTarget.classList.remove('on')
        mainSwiper1.autoplay.stop();
        slidePlay01.classList.add('on')        
    },
    play: function(e){
        e.currentTarget.classList.remove('on')
        mainSwiper1.autoplay.start();        
        slidePause01.classList.add('on')
    }
    
}

slidePause01.addEventListener("click", sliderControl.pause);
slidePlay01.addEventListener("click", sliderControl.play);


const mainRolling = {
    heightFunc: function(){
        let mainRolling2Height = document.querySelector('.rolling_banner02').offsetHeight
        document.documentElement.style.setProperty('--mainRolling2Height', mainRolling2Height + 'px')
    }
}

window.addEventListener("load", mainRolling.heightFunc);
window.addEventListener("resize", mainRolling.heightFunc);

const mainSwiper2 = new Swiper(".rolling_banner02", {
    autoplay: true,
    slidesPerView: 1,
    // direction: 'vertical',
    loop: true,
    pagination: false,
    navigation: false,
    pauseOnMouseEnter: true,
    pagination: {
        el: ".section01 .right .article_type03 .swiper-pagination",
        clickable: true
    },
});

const slidePause02 = document.querySelector('.section01 .right .slide_pause')
const slidePlay02 = document.querySelector('.section01 .right .slide_play')
const sliderControl2 = {
    pause: function(e){       
        e.currentTarget.classList.remove('on')
        mainSwiper2.autoplay.stop();
        slidePlay02.classList.add('on')        
    },
    play: function(e){
        e.currentTarget.classList.remove('on')
        mainSwiper2.autoplay.start();        
        slidePause02.classList.add('on')
    }
    
}

slidePause02.addEventListener("click", sliderControl2.pause);
slidePlay02.addEventListener("click", sliderControl2.play);


const mainSwiper3 = new Swiper(".swiper_banner03", {
    slidesPerGroup:3,
    slidesPerView: 5,
    spaceBetween: 20,
    speed: 1500,
    observer: true,
    observeParents: true,
    loopAdditionalSlides : 1,
    autoplay: false,
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
    scrollbar: {
        el: '.swiper-scrollbar',
        draggable: true,
    },
    breakpoints: {
        320: {
            centeredSlides: true,
            slidesPerView: 1.2,
            slidesPerGroup:1,
            spaceBetween: 15,
        },
        500: {
            centeredSlides: true,
            slidesPerView: 1.2,
            slidesPerGroup:1,
            spaceBetween: 20,
        },
        768: {
            centeredSlides: false,
            slidesPerView: 3,
            slidesPerGroup:3,
        //   spaceBetween: 40,
        },
        1280: {
            centeredSlides: false,
            slidesPerView: 4,
          //   spaceBetween: 40,
          },
        1440: {
            centeredSlides: false,
            slidesPerView: 5,
        //   spaceBetween: 50,
        },
      },
});



// sec03 tab
const mainBoardTabBtn = document.querySelectorAll('.tab_btn')
const mainBoardTabCon = document.querySelectorAll('.tab_con')
const mainBoardTabArrow = document.querySelectorAll('.section04 .btn_more_board')

mainBoardTabBtn.forEach((tab, idx)=> {
    tab.addEventListener('click', function(){
        mainBoardTabCon.forEach((inner)=> {
            inner.classList.remove('on')
        })

        mainBoardTabBtn.forEach((item)=> {
            item.classList.remove('on')
        })

        mainBoardTabArrow.forEach((item)=> {
            item.classList.remove('on')
        })

        mainBoardTabBtn[idx].classList.add('on')
        mainBoardTabCon[idx].classList.add('on')
        mainBoardTabArrow[idx].classList.add('on')
    })
})

//팝업 240205 추가
const mainPopup = document.querySelector('.popup_main');
if(mainPopup){
  scrollOn()
}

var popCloseButtons = document.querySelectorAll('.pop_close, .pop_bg, #sidebar .bg');
for (var j = 0; j < popCloseButtons.length; j++) {
  popCloseButtons[j].addEventListener('click', function () {
    var popupContainer = this.closest('.pop_cont');

      if (popupContainer) {
        scrollOff();}
  });
}