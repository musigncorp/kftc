function pcOnly() {
    var isMobile = {
        Android: function () {
            return navigator.userAgent.match(/Android/i) == null ? false : true;
        },
        iOS: function () {
            return ((navigator.userAgent.match(/iPhone|iPad|iPod/i) == null) && (navigator.maxTouchPoints == 0)) ? false : true;
        },
        any: function () {
            return (isMobile.Android() || isMobile.iOS());
        }
    };

    //입력된 링크를 전달하는 bridge function
    function outLink(link) {
        if (isMobile.any) {
            if (isMobile.Android() || isMobile.iOS()) {
                alert("pc에서 확인해주세요.");
                history.back();
            }
        }
    }
    outLink()

}
pcOnly()