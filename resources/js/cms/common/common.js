// js 규칙
// 변수 네이밍은 카멜 표기법(Camel Case)을 표준으로한다.


// lnb
const lnbMenu = document.querySelectorAll('.header .lnb .depth1 > li > a');
const lnbSubMenu = document.querySelectorAll('.header .lnb .depth2')
const nav = document.querySelector('.header .lnb')

lnbMenu.forEach((menus, idx) => {
  menus.addEventListener('mouseover', function () {
    lnbSubMenu.forEach((inner) => {
      inner.classList.remove('on')
    })
    lnbMenu.forEach((item) => {
      item.parentNode.classList.remove('on')
    })
    lnbSubMenu[idx].classList.add('on')
    lnbMenu[idx].parentNode.classList.add('on')
  })
  nav.addEventListener('mouseleave', function () {
    lnbSubMenu[idx].classList.remove('on')
    lnbMenu[idx].parentNode.classList.remove('on')
  })
})

let gnbDepth2HeightPx
let gnbDepth2Height
let idxBefore

const resizedMenu = {
  menu: function () {
    lnbMenu.forEach((menus, idx) => {
      if (window.innerWidth < 768) {
        lnbMenu[idx].parentNode.classList.remove("on");
      }
    });
  },
  moMenu: function () {
    // gnb - mo
    if (window.innerWidth < 768) {

      const moGnbMenu = document.querySelectorAll(".gnb .depth1 > li > p");
      const moGnbSubMenu = document.querySelectorAll(".gnb .depth2_wrap");

      moGnbMenu.forEach((menus, idx) => {
        menus.addEventListener("click", function (e) {
          // console.log('클릭테스트')
          moGnbMenu.forEach((item) => {
            item.parentNode.classList.remove("on");
          });
          moGnbSubMenu.forEach((item) => {
            item.classList.remove("on");
          });

          if (idxBefore == idx) {
            moGnbMenu[idx].parentNode.classList.remove("on");
            e.target.nextElementSibling.classList.remove("on");
            idxBefore = null
          } else {
            moGnbMenu[idx].parentNode.classList.add("on");
            e.target.nextElementSibling.classList.add("on");
            idxBefore = idx
          }
          gnbDepth2HeightPx = e.target.nextElementSibling.children[0].offsetHeight;
          gnbDepth2Height = pxToRem(gnbDepth2HeightPx);
          document.documentElement.style.setProperty("--gnbDepth2Height", gnbDepth2Height + "rem");
        });
      });
    }
  },
};

if (lnbMenu.length > 0) {
  // window.addEventListener("load", resizedMenu.menu);
  window.addEventListener("resize", resizedMenu.menu);
}

window.addEventListener("resize", resizedMenu.moMenu);
window.addEventListener("load", resizedMenu.moMenu);



// gnb
const gnbMenu = document.querySelectorAll('.gnb .depth2 > li > p');
const gnbSubMenu = document.querySelectorAll('.gnb .depth3_wrap')
let gnbDepth3Height
let gnbDepth3HeightPx
let gnbDepth2Height2
let idxBefore2
gnbMenu.forEach((menus, idx) => {
  menus.addEventListener("click", function (e) {
    gnbMenu.forEach((item) => {
      item.classList.remove("on");
    });
    gnbSubMenu.forEach((item) => {
      item.classList.remove("on");
    });
    if (idxBefore2 == idx) {
      gnbMenu[idx].classList.remove('on')
      e.target.nextElementSibling.classList.remove("on");
      idxBefore2 = null
    } else {
      gnbMenu[idx].classList.add('on')
      e.target.nextElementSibling.classList.add("on");
      idxBefore2 = idx
    }

    gnbDepth3HeightPx = e.target.nextElementSibling.children[0].offsetHeight;
    gnbDepth3Height = pxToRem(gnbDepth3HeightPx);
    document.documentElement.style.setProperty("--gnbDepth3Height", gnbDepth3Height + "rem");

    if (window.innerWidth < 768) {
      if (idxBefore2 == idx) {

        gnbDepth2Height2 = pxToRem(gnbDepth2HeightPx + gnbDepth3HeightPx);
        document.documentElement.style.setProperty("--gnbDepth2Height", gnbDepth2Height2 + "rem");
      } else {
        gnbDepth2Height2 = pxToRem(gnbDepth2HeightPx);
        document.documentElement.style.setProperty("--gnbDepth2Height", gnbDepth2Height2 + "rem");
      }
    }
  });
});

let scroll;

const header = document.querySelector(".header");

window.addEventListener("scroll", function () {
  if (window.scrollY !== undefined) {
    scroll = window.scrollY;
  } else {
    scroll = document.documentElement.scrollTop || document.body.scrollTop;
  }
  if (scroll > 0) {
    header.classList.add("on");
  } else {
    header.classList.remove("on");
  }

  let topBtn = document.querySelector(".btn_top");

  topBtn.addEventListener("click", function () {
    if (scroll > 0) {
      document.documentElement.scrollIntoView({
        block: "start",
        behavior: "smooth",
      });
    }
    // else {
    //   document.documentElement.scrollIntoView({
    //     block: "end",
    //     behavior: "smooth",
    //   });
    // }
  });
});

// select
const selectBtn = document.querySelectorAll(".select");
if (selectBtn) {
  const select = {
    click: function (e) {
      if (e.target.classList.contains("select") || e.target.parentElement.classList.contains("select")) {
        // if (e.target.classList=="select f_c") {
        if (!e.target.classList.contains('on')) {
          e.target.classList.add("on");
        } else if (e.target.parentElement.classList == "select f_c") {
          e.target.parentElement.classList.add("on")
        } else if (e.target.parentElement.classList == "select f_c on") {
          e.target.parentElement.classList.remove("on")
        } else {
          e.target.classList.remove("on");
        }
        e.preventDefault();
      } else {
        selectBtn.forEach((inner, idx) => {
          inner.classList.remove("on");
        })
      }
    },
  };
  document.addEventListener("click", select.click);
}

// select_li

const selectLi = document.querySelectorAll(".select li");
const email2 = document.querySelector("#email02");
const fakeSelected = document.querySelector('.fake_selected')
selectLi.forEach((inner, idx) => {
  inner.addEventListener('click', function (e) {
    let selectLiTxt = e.target.innerHTML
    let inputName
    if (email2) {
      // email2 있을시 값 할당
      if (e.target.parentNode.parentNode.parentNode.previousElementSibling.id == email2.id) {
        e.target.parentElement.parentElement.parentElement.previousElementSibling.value = selectLiTxt
        // console.log('yes')
      } else {
        // console.log('here')
        // input hidden 에 할당    
        e.target.parentElement.previousElementSibling.value = selectLiTxt
        e.target.parentElement.parentElement.previousElementSibling.previousElementSibling.previousElementSibling.innerHTML = selectLiTxt
        // fakeSelected.innerHTML = selectLiTxt
      }
    } else {
      if (!e.target.classList.contains('disabled')) {
        // input hidden 에 할당    
        // console.log('inner', inner)
        // console.log('selectLiTxt', selectLiTxt)
        // console.log('e.target.parentElement.parentElement.previousElementSibling.previousElementSibling.previousElementSibling', e.target.parentElement.parentElement.previousElementSibling.previousElementSibling.previousElementSibling)

        e.target.parentElement.previousElementSibling.value = selectLiTxt
        e.target.parentElement.parentElement.previousElementSibling.previousElementSibling.previousElementSibling.innerHTML = selectLiTxt

        // fakeSelected.innerHTML = selectLiTxt
      } else {

      }

    }

  })
})

// 셀렉트 박스 select 로 변경
const selectOption = document.querySelectorAll("select.select");
selectOption.forEach((inner, idx) => {
  // console.log(inner,'inner')
  inner.addEventListener('change', function (e) {
    let selectOptionTxt
    if (email2) {
      // email2 있을시 값 할당
      if (e.target.previousElementSibling.id == email2.id) {
        selectOptionTxt = inner.options[inner.selectedIndex].value
        email2.value = selectOptionTxt
        // console.log('yes22')
      } else {

      }
    }
    // else {
    //   if (!e.target.classList.contains('disabled')) {
    //     // input hidden 에 할당    
    //     // console.log('inner', inner)
    //     // console.log('selectLiTxt', selectLiTxt)
    //     // console.log('e.target.parentElement.parentElement.previousElementSibling.previousElementSibling.previousElementSibling', e.target.parentElement.parentElement.previousElementSibling.previousElementSibling.previousElementSibling)
    //     e.target.parentElement.previousElementSibling.value = selectLiTxt
    //     e.target.parentElement.parentElement.previousElementSibling.previousElementSibling.previousElementSibling.innerHTML = selectLiTxt
    //     // fakeSelected.innerHTML = selectLiTxt
    //   } else {

    //   }

    // }
  })
})

// gnbButton
const gnbButton = document.querySelector(".btn_gnb");
const gnb = document.querySelector(".gnb");

const gnbFunc = {
  click: function (e) {
    if (gnb.classList.contains("on")) {
      gnb.classList.remove("on");
      gnbButton.classList.remove("on");
      header.classList.remove("active")
    } else {
      gnb.classList.add("on");
      gnbButton.classList.add("on");
      header.classList.add("active")
    }
  },
};
gnbButton.addEventListener("click", gnbFunc.click);

// px -> vw 
function pxToVw(pxValue) {
  var screenWidth = window.innerWidth;
  var vwValue = (pxValue / screenWidth) * 100;

  return vwValue;
}

// px -> rem
function pxToRem(pxValue) {
  var remValue = pxValue / 9

  return remValue;
}


// tab

const tabItem2 = document.querySelectorAll('.tab_li2')
const tabInner2 = document.querySelectorAll('.tab_content_li2')
tabItem2.forEach((tab, idx) => {
  tab.addEventListener('click', function () {
    tabInner2.forEach((inner) => {
      inner.classList.remove('on')
    })

    tabItem2.forEach((item) => {
      item.classList.remove('on')
    })

    tabItem2[idx].classList.add('on')
    tabInner2[idx].classList.add('on')
  })
})


const numsExp = /^[0-9]*$/;

function recNumsChk(obj) {
  let issueNum = obj.value;


  if (!numsExp.test(issueNum)) {
    alert('')
    return false;
  } else {


    return true;
  }
}



/** 숫자 허용 정규식*/
const inputNum = (target) => {
  target.value = target.value
    .replace(/[^0-9]/, '')
}

/** 원화표시 정규식 */
const addCommas = (target) => {
  // 숫자 형식으로 변환하고 쉼표를 추가
  target.value = target.value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}


/** 유리수 정규식 */
const inputNum2 = (target) => {
  target.value = target.value.replace(/[^0-9.]+/g, '');
  // 두 번 이상의 소수점을 방지하기 위해 추가 검증
  target.value = target.value.replace(/^(\d*\.\d*)\..*$/, '$1');
}



// 팝업창 
const popupBtns = document.querySelectorAll('.btn_popup');
popupBtns.forEach(function (popupBtn) {
  popupBtn.addEventListener('mouseenter', function () {
    this.classList.add("act")
  });

  popupBtn.addEventListener('mouseleave', function () {
    this.classList.remove("act")
  });
});

// 팝업 시 화면 스크롤 방지
const body = document.querySelector('body');
let scrollPosition = 0;

function scrollOn(n) {
  if (n) {
    window.scrollTo(0, n);
  }
  scrollPosition = window.pageYOffset;
  body.style.overflow = 'hidden';
  body.style.position = 'fixed';
  body.style.top = `-${scrollPosition}px`;
  body.style.width = '100%';
}

function scrollOff() {
  body.style.removeProperty('overflow');
  body.style.removeProperty('position');
  body.style.removeProperty('top');
  body.style.removeProperty('width');
  window.scrollTo(0, scrollPosition);
}

// const mainPopup = document.querySelector('.popup_main');
// if(mainPopup){
//   scrollOn()
// }

// 팝업 열기
var popOpenButtons = document.querySelectorAll('.btn_popup');
var scrollBoxs = document.querySelectorAll(".scroll_box");

for (var i = 0; i < popOpenButtons.length; i++) {
  popOpenButtons[i].addEventListener('click', function (e) {
    e.preventDefault();
    var popupId = this.getAttribute('data-popup');
    var popup = document.getElementById(popupId);
    if (popup) {
      popup.style.display = 'block';
      popup.classList.add('act');
      popup.closest('.popup_wr').classList.add('on');
      scrollBoxs.forEach(function (scrollBox) {
        scrollBox.scrollTop = 0;
      });
      scrollOn()
    }
  });
}

// 팝업 닫기
var popCloseButtons = document.querySelectorAll('.pop_close, .pop_bg, #sidebar .bg');
for (var j = 0; j < popCloseButtons.length; j++) {
  popCloseButtons[j].addEventListener('click', function () {

    var popupContainer = this.closest('.pop_cont');

    if (popupContainer) {
      popupContainer.style.display = 'none';
      popupContainer.classList.remove('act');
      var parentContainer = this.closest('.cont');
      // console.log(parentContainer)
      if (parentContainer) {
        var activePopups = parentContainer.querySelectorAll('.act');
        scrollOff();
        if (activePopups.length === 0) {
          parentContainer.classList.remove('on');
        }
      }
    }
  });
}

// 팝업 내용 전송 버튼 (닫기 기능)
const popSendButtons = document.querySelector('.pop_cont button.send_cont');
if (popSendButtons) {
  // console.log('test~~~~~~~~~')
  popSendButtons.addEventListener('click', function () {
    var popupContainer = this.closest('.pop_cont');
    popupContainer.style.display = 'none';
    popupContainer.classList.remove('act');
    var parentContainer = this.closest('.cont');
    // console.log(parentContainer)
    if (parentContainer) {
      var activePopups = parentContainer.querySelectorAll('.act');
      scrollOff();
      if (activePopups.length === 0) {
        parentContainer.classList.remove('on');
      }
    }
  });
}

// 사파리 100vh 관련 스크립트
let updateViewportHeight = () => {
  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty("--vh", `${vh}px`);
};

window.addEventListener("orientationchange", updateViewportHeight);
window.addEventListener("resize", updateViewportHeight);
window.addEventListener("touchend", updateViewportHeight);


// calender
const calender = document.getElementById('datepicker')
const calenderStart = document.getElementById('datepickerStart')
const calenderEnd = document.getElementById('datepickerEnd')
const allcalenderStart = document.getElementById('allDatepickerStart')
const allcalenderEnd = document.getElementById('allDatepickerEnd')
const calenderMonthlyStart = document.getElementById('datepickerMonthlyStart')
const calenderMonthlyEnd = document.getElementById('datepickerMonthlyEnd')




if (calender || calenderStart || calenderEnd || allcalenderStart || allcalenderEnd || calenderMonthlyStart || calenderMonthlyEnd) {
  $("#datepicker, #datepickerStart, #datepickerEnd, #allDatepickerStart, #allDatepickerEnd").datepicker({
    changeMonth: true,
    changeYear: true,
    nextText: '다음 달',
    prevText: '이전 달',
    language: 'ko',
    beforeShow: function (input) {

      var i_offset = jQuery(input).offset(); // 클릭된 input의 위치값 체크
      var i_width = jQuery(input).outerWidth(); // 클릭된 input의 크기
      var i_height = jQuery(input).outerHeight(); // 클릭된 input의 크기
      // console.log(jQuery(input).outerWidth())
      setTimeout(function () {
        if (window.innerWidth < 1400) {
          jQuery("#ui-datepicker-div").css({
            "top": i_offset.top + i_height + 10
          });
          jQuery("#ui-datepicker-div").css({
            "left": i_offset.left
          });
        } else {
          jQuery("#ui-datepicker-div").css({
            "top": i_offset.top
          });
          jQuery("#ui-datepicker-div").css({
            "left": i_offset.left + i_width + 10
          });
        }

        // datepicker의 div의 포지션을 강제로 클릭한 input 위치로 이동시킨다.
      })
    }
  });
  $('#datepicker').datepicker('option', 'minDate', '0'); //오늘이후 선택가능
  $('#datepicker').datepicker('setDate', '+1D'); //(-1D:하루전, -1M:한달전, -1Y:일년전), (+1D:하루후, -1M:한달후, -1Y:일년후)
  $('#datepickerStart').datepicker('setDate', '0'); //(-1D:하루전, -1M:한달전, -1Y:일년전), (+1D:하루후, -1M:한달후, -1Y:일년후)
  $('#datepickerEnd').datepicker('setDate', '0'); //(-1D:하루전, -1M:한달전, -1Y:일년전), (+1D:하루후, -1M:한달후, -1Y:일년후)
  $('#allDatepickerStart').datepicker('setDate', '0'); //(-1D:하루전, -1M:한달전, -1Y:일년전), (+1D:하루후, -1M:한달후, -1Y:일년후)
  $('#allDatepickerEnd').datepicker('setDate', '0'); //(-1D:하루전, -1M:한달전, -1Y:일년전), (+1D:하루후, -1M:한달후, -1Y:일년후)


  // 미래 날짜 선택 불가능 시 데이트 피커
  $('#datepickerStart').datepicker("option", "maxDate", $("#datepickerEnd").val());
  $('#datepickerEnd').datepicker("option", "maxDate", 'today');
  $('#datepickerEnd').datepicker("option", "onClose", function (selectedDate) {
    $("#datepickerStart").datepicker("option", "maxDate", selectedDate);
  });
  // 최대 3개월 선택 시 해당 클래스 추가
  $('.ut_3month').datepicker(
    "option", "minDate", '-3M', "maxDate", '0'
  );
  // 최대 17개월 선택 시 해당 클래스 추가
  $('.ut_17month').datepicker(
    "option", "minDate", '-17M', "maxDate", '0'
  );

  // calender monthly
  $("#datepickerMonthlyStart, #datepickerMonthlyEnd").datepicker({
    showOn: 'both',
    buttonText: "",
    currentText: "오늘",
    changeMonth: true,
    changeYear: true,
    showButtonPanel: true,
    // yearRange: 'c-99:c+0',
    showOtherMonths: true,
    selectOtherMonths: true,
    closeText: "선택",
    dateFormat: "yy-mm",
    minDate: "-99Y",
    maxDate: "0",
    onClose: function (dateText, inst) {
      var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
      var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
      $(this).datepicker("option", "defaultDate", new Date(year, month, 1));
      $(this).datepicker('setDate', new Date(year, month, 1));
    },
    // beforeShow: function () {
    //   var selectDate = $("#datepickerMonthly").val().split("-");
    //   var year = Number(selectDate[0]);
    //   var month = Number(selectDate[1]) - 1;
    //   $(this).datepicker("option", "defaultDate", new Date(year, month, 1));
    // },
    beforeShow: function (input) {

      var i_offset = jQuery(input).offset(); // 클릭된 input의 위치값 체크
      var i_width = jQuery(input).outerWidth(); // 클릭된 input의 크기
      var i_height = jQuery(input).outerHeight(); // 클릭된 input의 크기
      // console.log(jQuery(input).outerWidth())
      setTimeout(function () {
        if (window.innerWidth < 1400) {
          jQuery("#ui-datepicker-div").css({
            "top": i_offset.top + i_height + 10
          });
          jQuery("#ui-datepicker-div").css({
            "left": i_offset.left
          });
        } else {
          jQuery("#ui-datepicker-div").css({
            "top": i_offset.top
          });
          jQuery("#ui-datepicker-div").css({
            "left": i_offset.left + i_width + 10
          });
        }
      })
    }
  });

  // 최대 3년전 선택 시 해당 클래스 추가
  $('.ut_3year').datepicker(
    "option", "minDate", '-36M', "maxDate", 'today'
  );
  // 최대 17개월전 선택 시 해당 클래스 추가
  $('.ut_17month_m').datepicker(
    "option", "minDate", '-17M', "maxDate", 'today'
  );

  $('#datepickerMonthlyStart').datepicker('setDate', '0'); //(-1D:하루전, -1M:한달전, -1Y:일년전), (+1D:하루후, -1M:한달후, -1Y:일년후)
  $('#datepickerMonthlyEnd').datepicker('setDate', '0'); //(-1D:하루전, -1M:한달전, -1Y:일년전), (+1D:하루후, -1M:한달후, -1Y:일년후)

}

//웹표준 테이블 캡션 추가
// const dateSelect = document.querySelector('.ui-datepicker-title select');
// console.log(dateSelect)
// dateSelect.classList.add('selfakdj')



// 토글버튼
const toggleBtns = document.querySelectorAll('.toggle_btn');
const toggleContents = document.querySelectorAll('.toggle_contents_box');

toggleBtns.forEach((toggleBtn, index) => {
  toggleBtn.addEventListener('click', () => {
    toggleBtn.classList.toggle('active');
    toggleContents[index].classList.toggle('active');
  });
});


// 조회기관 이용기관 코드 유효성 검사
function agencyCheck() {

  const comNum = document.querySelector('#com_num')
  const comNumVal = comNum.value;
  comNum.value = comNumVal.replace(/([^0-9])/g, '');

  if (comNumVal !== '' || comNum.classList.contains("required_chk")) {
    if (comNumVal.length !== 10 || comNumVal.substr(0, 2) !== '99') {
      alert("기관코드의 형식이 올바르지 않습니다. 기관코드는 99로 시작하는 숫자 10자리입니다.");
      comNum.focus()
      return false;
    }
  }
}

function agencyCheck02() {
  const stDatepicker = document.querySelector('#allDatepickerStart')
  const edDatepicker = document.querySelector('#allDatepickerEnd')
  const comNum = document.querySelector('#com_num')
  const comNumVal = comNum.value;
  comNum.value = comNumVal.replace(/([^0-9])/g, '');
  // console.log(stDatepicker)
  if (!stDatepicker.value) {
    alert('시작일을 선택해주세요')
    stDatepicker.focus()
    return false
  } else if (!edDatepicker.value) {
    alert('종료일을 선택해주세요')
    edDatepicker.focus()
    return false
  } else if (stDatepicker.value == edDatepicker.value) {
    return true
  } else {
    // console.log(comNumVal.length)
    if (comNumVal.length !== 10 || comNumVal.substr(0, 2) !== '99') {
      alert('과도한 시스템 부하로 당일을 제외한 일정기간동안의 전체 조회는 불가')
      comNum.focus()
      return false;
    }
    if (comNumVal !== '') {
      if (comNumVal.length !== 10 || comNumVal.substr(0, 2) !== '99') {
        alert('과도한 시스템 부하로 당일을 제외한 일정기간동안의 전체 조회는 불가')
        comNum.focus()
        return false;
      }
    }
  }
}

// 첨부파일
const attachFileWrap = document.querySelector('.attach_file_wrap');

if (attachFileWrap) {
  // 첨부파일 영역 라디오탭
  const docu01RadioButton = document.getElementById('docu01');
  const docu02RadioButton = document.getElementById('docu02');
  const docu03RadioButton = document.getElementById('docu03');
  const attachNoticeWrap = document.querySelector('.attach_notice_wrap');
  if (docu03RadioButton) {
    docu03RadioButton.addEventListener('change', updateAttachmentNotice);
  }
  if (docu01RadioButton) {
    docu01RadioButton.addEventListener('change', updateAttachmentNotice);
  }
  if (docu02RadioButton) {
    docu02RadioButton.addEventListener('change', updateAttachmentNotice);
  }

  function updateAttachmentNotice() {
    if (docu03RadioButton.checked) {
      attachNoticeWrap.classList.add('active');
    } else {
      attachNoticeWrap.classList.remove('active');
    }
  }

  // 첨부파일 추가 삭제 버튼
  const addFileBtn = document.querySelector('.btn.add');
  const delFileBtn = document.querySelector('.btn.del');
  const originElements = document.querySelectorAll('.file_box.origin');
  if (addFileBtn) {
    addFileBtn.addEventListener('click', function () {
      const addActElements = document.querySelectorAll('.file_box.add_act');
      if (addActElements.length > 0) {
        const nextAddActElement = addActElements[addActElements.length - 1].nextElementSibling;
        if (nextAddActElement) {
          nextAddActElement.classList.add('add_act');
        }
      }
      if (addActElements[+0].classList.contains('add_act')) {
        document.querySelector('.btn.del').style.display = "block";
      } else {
        document.querySelector('.btn.del').style.display = "none";
      }
    });
  }
  if (delFileBtn) {
    delFileBtn.addEventListener('click', function () {
      const addActElements = document.querySelectorAll('.file_box.add_act');
      // console.log(addActElements.length, 'addActElements.length')
      // console.log(originElements.length, 'originElements.length')
      if (addActElements.length > originElements.length) {
        const lastAddActElement = addActElements[addActElements.length - 1];
        if (!lastAddActElement.classList.contains('origin')) {
          lastAddActElement.classList.remove('add_act');
        }
        document.querySelector('.btn.del').style.display = "block";
        if ((addActElements.length - 1) == originElements.length) {
          // console.log('test')
          document.querySelector('.btn.del').style.display = "none";
        }
      }
      // if (addActElements[+1].classList.contains('add_act')){
      // document.querySelector('.btn.del').style.display="block";
      // } else {
      //   document.querySelector('.btn.del').style.display="none";
      // }
    });
  }

  // 첨부파일 파일명 가져오기
  const fileInputs = document.querySelectorAll('.input_txt_type02.attach_file');
  const fakeInputDivs = document.querySelectorAll('.fake_input');
  if (fileInputs) {
    fileInputs.forEach((input, index) => {
      input.addEventListener('change', function () {
        if (input.files.length > 0) {
          fakeInputDivs[index].textContent = input.files[0].name;
        } else {
          fakeInputDivs[index].textContent = "파일이름.pdf"; // 파일 선택이 해제된 경우 기본 파일 이름으로 변경
        }
      });
    });
  }

  function fmenulist_submit(f) {
    return true;
  }
}

//웹접근성 버튼에 value 넣기
document.querySelectorAll('.ui-datepicker-trigger').forEach(function (element) {
  element.textContent = '날짜 선택';
});

//  납부자 번호 체계 테이블 초기화 버튼 스크립트
const numDataTable = document.querySelector('.num_data_table')
if (numDataTable) {
  const resetBtns = document.querySelectorAll('.reset_btn');
  resetBtns.forEach((resetBtn) => {
    resetBtn.addEventListener('click', function () {
      const line = this.closest('.line'); // 해당 줄의 부모 tr 엘리먼트
      const inputFields = line.querySelectorAll('input[type="text"]'); // 해당 줄의 모든 텍스트 입력 필드
      const radioButtons = line.querySelectorAll('input[type="radio"]'); // 해당 줄의 모든 라디오 버튼

      // 모든 텍스트 입력 필드를 빈 값으로 설정
      inputFields.forEach(input => {
        input.value = '';
      });

      // 모든 라디오 버튼을 해제 상태로 설정
      radioButtons.forEach(radio => {
        radio.checked = false;
      });
    });
  });
}



// 빈칸 입력 알럿창
function saveToSessionStorage() {
  window.sessionStorage.clear();

  const applyService = document.querySelector('.sub02_2');
  if (applyService) {
    let title;
    const agreementSections = document.querySelectorAll('.agree_cont');

    for (let i = 0; i < agreementSections.length; i++) {
      const radioBox = agreementSections[i].querySelector('.radio_box');
      const agreeRadio = radioBox.querySelector('input[type="radio"][id^="agree"]:checked');
      const disagreeRadio = radioBox.querySelector('input[type="radio"][id^="disagree"]:checked');

      if (!agreeRadio && disagreeRadio) {
        // 동의하지 않은 라디오 버튼이 있는 경우
        alert('이용약관 동의는 필수입니다.');
        window.scrollTo(0, agreementSections[i].offsetTop - 100);
        return false;
      }
    }

  }

  const inputEl = document.querySelectorAll('.required_label input');
  for (let idx = 0; idx < inputEl.length; idx++) {
    const value = inputEl[idx].value.trim();
    if (!value) {
      title = inputEl[idx].closest('.required_label').querySelector('span');
      alert(title.innerHTML + '을(를) 입력해주세요');
      window.scrollTo(0, (title.offsetTop - 200));
      inputEl[idx].focus()
      return false;
    }
  }

  const textareaEl = document.querySelectorAll('.required_label textarea');
  for (let idx = 0; idx < textareaEl.length; idx++) {
    const value = textareaEl[idx].value.trim();
    if (!value) {
      title = textareaEl[idx].closest('.required_label').querySelector('span');
      alert(title.innerHTML + '을(를) 입력해주세요');
      window.scrollTo(0, (title.offsetTop - 200));
      textareaEl[idx].focus()
      return false;
    }
  }

}


// 장애대응
const disOrderMenu = document.querySelector('#dis_order')

function disOrder() {

  alert('장애시 파일송수신은 CMSPro를 통해 가능하며,\n세부내용은 담당자 앞 문의바랍니다.\n\n(주간) 납부기획팀 : 02-531-1622~1623\n(야간) IT운영3팀 : 02-531-1343~1345')

}


// 사파리 체크
if (isSafari()) {


  // 사파리 브라우저일 때 특정 작업 수행
  var tableHeaders = document.querySelectorAll('table th');
  for (var i = 0; i < tableHeaders.length; i++) {
    tableHeaders[i].classList.add('safari');
  }
} else {

}

// 사파리 브라우저 여부를 확인하는 함수
function isSafari() {
  return /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
}
// 240228 뮤자인 태일
function printPage() {
  window.print();
}